# Documentation for Beginners

```
Ola. I decided to try my hand at writing a little documentation
for IT spokes beginners.I hope it will turn out pretty good.
And if you find a grammatical error, please let me know,
I would be glad for your help. Thank you.
```

```
Ола. Я решил попробовать свои силы в написании небольшой документации
для начинающих IT-спикеров. Надеюсь, получится неплохо.
И если вы найдете какую либо ошибку, пожалуйста, дайте мне знать,
Я буду рад вашей помощи. Спасибо.
```